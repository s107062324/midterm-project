# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : **Chatroom**
* Key functions (add/delete)
    1. 信箱登入/註冊/登出
    2. google登入
    3. 建立私人聊天室
    4. 自己聊天室
    5. 大眾聊天室
    6. 資料讀寫
    7. 載入聊天紀錄
    8. 聊天氣泡框
    9. 聊天通知
    10. RWD
    11. CSS動畫
    
* Other functions (add/delete)
    1. 搜尋用戶
    2. 更換大頭貼
    3. 更換用戶名稱
    4. 用戶介面
    5. 寄送密碼重設郵件
    6. <font color=red>**已讀功能**</font>

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

## 作品網址：https://chatroom-de704.web.app/

## Components Description : 
1. 登入 / 註冊 / google登入 : 
    >在主頁面可以做登入跟註冊，也可以用google帳號登入
    
    ![](https://i.imgur.com/qV0GcBu.png =400x250)


2. 私人 / 大眾 / 自己的聊天室：
    >左邊是用戶欄，只要有登入過的用戶都會是彼此的好友，點擊用戶欄的chat便可以開始聊天，<font color=black>**用戶本身也可以跟自己聊天！不用擔心沒朋友 : )**</font>。用戶欄中最上方是為自己的資料，是固定不會動的。
    
    >右邊是聊天室，在下方可以輸入文字(一開始要先選定要聊天的地點，否則無法傳送訊息)。
    
    ![](https://i.imgur.com/39eXfdY.png =400x250)

3. 載入聊天紀錄：
    >只要按下chat或是public，就會自動切換到正確的聊天室，並且會自動載入之前所有的聊天訊息。
    
    >聊天室上方也會有對方的email(如果對方有username的話，則會是對方的username)，方便知道自己再跟誰對話。

4. 聊天氣泡框 : 
    >會區分成對方傳來跟自己發出的訊息。若是自己傳出的會靠右方，對方傳的則靠左方，顏色上也有區別。
    
    ![](https://i.imgur.com/fqIB7Mz.png =300x100)

5. 聊天通知：
    >當其他用戶傳送新訊息來時，會從右邊跑出通知 (除了public跟自己的聊天室)。

    ![](https://i.imgur.com/fv8ejgG.png =300x100)

 
6. RWD：
    >每個不同的頁面都有做RWD，讓頁面在不同裝置或不同大小時做排版上的變化。
    >
    ex：chatroom
    ![](https://i.imgur.com/t7IKvTt.png =200x200)

7. CSS動畫：
    >在登入畫面有一個咖啡機的動畫。
    
    ![](https://i.imgur.com/3IDZOaQ.gif =250x200)


## Other Functions Description : 
1. 搜尋用戶
    >在聊天室左上方有個搜尋功能，輸入其他用戶的email就可以直接到跟對方的聊天室中。<font color=black>**如果沒有朋友他會發出警告 : (**</font>
    
    ![](https://i.imgur.com/Nwc1lcB.png =300x50)

2. 設定(3種功能)：
    用戶的資料欄中中有個setting button，按下去後有三種功能可以使用。
    ![](https://i.imgur.com/tB21NIL.png =300x120)
    
    (1) 更換大頭貼/更換用戶名稱
    >當按下Profile按鈕時，會跳到設定用戶資料的頁面。
    >按下上面的頭像，可以選取自己的圖片當作頭像 (當你更改時頭像，其他用戶會立即收到你的改變)。
    
    >下面的username是輸入你自己想要的用戶名稱，如果對方有設定username的話，與對方聊天室名稱則會是對方的username。
    
    >當輸入及選擇完後，按下save儲存後，然後要再按下back回到聊天室的頁面。

    ![](https://i.imgur.com/jbTGMMr.png =200x250)
    
    (2) 寄送密碼重設郵件
    >當按下reset Password時，會自動幫你登出，然後會寄一封信到信箱，為是否要重設密碼的確認信。
    
    (3) 登出
    > 當按下登出後，會先確認是否真的要登出，如果按下確認後會跳回登入的畫面。
    
3. <font color=red>**已讀功能**</font>
    ><font color=black>就像其他應用程式的功能，當對方有讀取你傳的訊息時，他會在文字框旁邊立即顯示已讀，**這樣至少會是已讀不回，而不是不讀不回**。</font>
    
    >在大眾聊天室中則沒有已讀的功能。

## Security Report (Optional)
1. database rule
    >在database的rule中設定，如果不是註冊過的用戶則不能輸入或更改資訊。
    
2. Deal with messages when sending html code
    >在輸入文字的地方做了特殊處理，才能避免輸入html相關時不能顯示出來。
    
    ![](https://i.imgur.com/LJOfurJ.png =250x70)
