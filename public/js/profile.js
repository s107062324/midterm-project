var username = document.getElementById("username");
var btnSave = document.getElementById("btnSave");
var btnImage = document.getElementById("profile-image1");
var upload = document.getElementById("profile_image_upload");
var Back = document.getElementById("btnBack");
var filename = '';

function init(){
    firebase.auth().onAuthStateChanged(function(user) {
        if(user){
            var Username = '';
            var Filename = '';
            username.value = "";
            filename = ''
            if(user.picture == ''){
                btnImage.src = "img/" + user.picture;
            }
            else btnImage.src = "img/unknown.png";
            if(username.value != ''){
                Username = username.value;
            }
            else Username = user.username;
            if(filename != ''){
                Filename = filename;
            }
            else Filename = user.picture;
            
            btnSave.addEventListener('click', function(){
                firebase.database().ref('chat_list/' + user.uid).update({
                    "user": user.email,
                    "username": username.value,
                    "picture": filename
                });                
                username.value = "";
                filename = "";
                //window.location.href = 'chatroom.html';
            });

            Back.addEventListener('click',function(){
                window.location.href = 'chatroom.html';
            })

            // to trigger the file button
            $('#profile-image1').click(function(){
                $('#profile_image_upload').trigger('click');
            })

        }
    });
}

function changePicture(fileInput){
    var files = fileInput.files;
    filename = '';
    for(var i = 0; i < files.length; i++){
        filename += files[i].name;
    }

    btnImage.src = "img/" + filename;
}

window.onload = function() {
    init();
};