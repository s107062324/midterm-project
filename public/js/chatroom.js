var userImg = document.getElementById("user_img");
var now_chat_person = 'undefined';
var Public = false;
var Check;

function init() {
    if(Notification.permission == 'default' || Notification.permission == 'undefined'){
        Notification.requestPermission();
    }
    firebase.auth().onAuthStateChanged(function(user) {
        var UserName = document.getElementById("username");
        // Check user login
        if (user) {
            user_email = user.email;
            UserName.innerHTML = user_email;
            now_chat_person = user_email;
            var user_uid = user.uid;

            // Logout
            var logout_btn = document.getElementById('logout-btn');
            logout_btn.onclick = function() {
                var user = firebase.auth();
                var result = confirm("Are you sure to leave?");
                if(result){
                    now_chat_person = 'undefined';
                    firebase.database().ref('chat_list/' + user_uid).update({
                        'chat_with': now_chat_person
                    })
                    user.signOut().then(function(){
                        alert('success logout');
                        window.location.href = "index.html";
                    })
                    .catch(function(){
                        alert('QAQ');
                    })
                }
            }
            /*------- set user profile --------*/
            profile_btn.onclick = function(){
                window.location.href = 'profile.html';
            }

            /*------- add the new email -------*/
            var chatRef = firebase.database().ref('chat_list/');
            var flag = 0;
            var user_picture = 'undefined';
            var user_name = 'undefined';

            chatRef.once('value')
            .then(function(snapshot) {
                for(let i in snapshot.val()){
                    if(snapshot.val()[i].user == user_email){
                        flag = 1;
                        user_picture = snapshot.val()[i].picture;
                        user_name = snapshot.val()[i].username;
                    }
                }

                /*------- update user info -------*/
                if(user_picture == 'undefined' || user_picture == ''){
                    userImg.src = "img/unknown.png";
                }
                else userImg.src = "img/" + user_picture;
                
                if(flag == 0){ // means this is a new user
                    firebase.database().ref('chat_list/' + user_uid).set({
                        'user': user_email,
                        'username': user_name,
                        'picture': user_picture,
                        'chat_with': now_chat_person,
                        'username': user_name
                    })
                }
            })
            .catch(e => console.log(e.message));
        } else {
            // It won't show any post if not login
            document.getElementById('post_list').innerHTML = "";
        }
    });

    // --------------- sent new message ----------------------
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    

    post_btn.addEventListener('click', function(){
        var user = firebase.auth().currentUser;
        if (post_txt.value != "" && now_chat_person != 'undefined') {
            var Now = Get_Time();
            var key;

            key = firebase.database().ref('com_list').push({
                "email": user_email,
                "data": converthtml(post_txt.value),
                "to": now_chat_person,
                "time": Now,
                "read": false
            }).key;
            firebase.database().ref('com_list/' + key).update({
                "key": key
            })

            post_txt.value = "";
        }
        var mesgs = document.getElementById('mesgs');
        mesgs.scrollTop = mesgs.scrollHeight;
    });

    /*---------------------the chat room--------------------------*/
    // the person who registered
    var chatRef = firebase.database().ref('chat_list/');
    // List for store posts html
    var total_chat = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    chatRef.once('value')
    .then(function(snapshot) {
        var user = firebase.auth().currentUser;
        /// Add listener to update new chat people
        chatRef.on('child_added', function(data){
            second_count += 1;
            if (second_count > first_count){
                var childData = data.val();
                var img = childData.picture;
                if(img == 'undefined' || img == '') img = 'img/unknown.png';
                else img = 'img/' + img;

                var str_before_clientname = "<div class='chat_list active_chat' id='chat_people'><div class='chat_people'><div class='chat_img'> <img src='"+ img +"'></div><div class='chat_ib'><h5 id='clientname'>";
                var str_after_clientname = "</h5><p id='chatcmt'></p>";
                var str_clientname = "</div></div></div>";
                var str_btn = "<button class='blue-pill' id='button' onclick='Changeperson(this,1)' value='"+data.val().user+"'>chat</button>";
                if(childData.email != user.email){
                    total_chat[total_chat.length] = str_before_clientname + childData.user + str_after_clientname + str_btn + str_clientname;
                }
                document.getElementById('chat_people').innerHTML = total_chat.join('');
            }
        });
        chatRef.on('child_changed',function(data){
            Chat(0);
        });
    })
    .catch(e => console.log(e.message));
    /*------------------------------------------------------------------------*/
}

function Changeperson(x, flag1){
    var search_text = document.getElementById('search_text');
    var search = document.getElementById('search');
    var user = firebase.auth().currentUser;

    //--------------------- search friend --------------------------*/
    if(flag1 == 0){
        if(search_text.value != ''){
            var flag2 = 0;
            var chatRef1 = firebase.database().ref('chat_list/');
            chatRef1.once('value')
            .then(function(snapshot) {
                for(let i in snapshot.val()){
                    if(snapshot.val()[i].user == search_text.value){
                        now_chat_person = search_text.value;
                        alert("chat with " + now_chat_person);
                        if(snapshot.val()[i].username != '' || snapshot.val()[i].username!='undefined'){
                            room_name = snapshot.val()[i].username;
                        }
                        else room_name = now_chat_person;
                        document.getElementById('room_title').innerHTML = "<p id='room_name'>"+ room_name +"</p>";
                        firebase.database().ref('chat_list/' + user.uid).update({
                            'chat_with': now_chat_person
                        })
                        flag2 = 1;
                    }
                }
                if(flag2 == 0){
                    if(search_text.value == 'public'){
                        search_text.value = '';
                        return;
                    }
                    alert("你確定他是你朋友嗎? 看來他不這麼認為 : ( ");
                    return;
                }
                search_text.value = ''
            })
            .catch(e => console.log(e.message));
        }
        else return;
    }
    else if(flag1 == 1){
        now_chat_person = x.value;
        firebase.database().ref('chat_list/' + user.uid).update({
            'chat_with': now_chat_person
        })
        if(now_chat_person == "public"){
            alert("chat in public");
            document.getElementById('room_title').innerHTML = "<p id='room_name'>"+ now_chat_person +"</p>";
            Public = true;
        }
        else {
            alert("chat with " + now_chat_person);
            document.getElementById('room_title').innerHTML = "<p id='room_name'>"+ now_chat_person +"</p>";
            Public = false;
        }
    }

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_chat = 0;
    // Counter for checking when to update new post
    var second_chat = 0;

    postsRef.once('value')
    .then(function(snapshot){
        var msg_list = document.getElementById('msg_list');
        
        msg_list.innerHTML = total_post.join('');     
        
        /// Add listener to update new post
        postsRef.on('child_added', function(data){
            second_chat += 1;
            if (second_chat > first_chat) {
                var childData = data.val();
                var display;
                Chat(1); // to get the data of chat_people
                Read(1);
                //alert("hi");

                if(childData.read == false || now_chat_person == 'public'){
                    display = ' ';
                }
                if(childData.read == true){
                    display = '已讀';
                }

                // The html code for post
                var before_user_cmt = "<div class='my-2 rounded box-shadow'></h6><div class='Media_r'><span class='time_date_l'><span class='read_l' id='"+childData.key+"'>"+display+"</span>"
                var mid_user_cmt = "</span><p class='sent_msg media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                var after_user_cmt = "</p></div></div>\n";

                var before_client_cmt = "<div class='my-2 rounded box-shadow'></h6><div class='Media_l'><p class='received_msg media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                var mid_client_cmt = "</p><span class='time_date_r'><span class='read_r' id='"+childData.key+"'></span>";
                var after_client_cmt = "</span></div></div>\n";

                var check1 = (childData.to == now_chat_person && childData.email == user.email);
                var check2 = (childData.email == now_chat_person && childData.to == user.email);
                if(Public && childData.to == now_chat_person){
                    first_chat++;
                    if(childData.email != user_email){
                        total_post[total_post.length] = before_client_cmt + childData.email + "</strong>" + childData.data + mid_client_cmt + childData.time + after_client_cmt;
                    }
                    else{
                        total_post[total_post.length] = before_user_cmt + childData.time + mid_user_cmt + childData.email + "</strong>" + childData.data + after_user_cmt;
                    }
                }
                else if(check1 || check2){
                    first_chat++;
                    if(childData.email == user_email){
                        total_post[total_post.length] = before_user_cmt + childData.time + mid_user_cmt + childData.email + "</strong>" + childData.data + after_user_cmt;
                    }
                    else{
                        total_post[total_post.length] = before_client_cmt + childData.email + "</strong>" + childData.data + mid_client_cmt + childData.time + after_client_cmt;
                    }
                }
                document.getElementById('msg_list').innerHTML = total_post.join('');
                var mesgs = document.getElementById('mesgs');
                mesgs.scrollTop = mesgs.scrollHeight;

                // make the chrome notification
                if(Notification.permission === "granted"){
                    var time = Get_Time();
                    if(childData.email != user.email && childData.to == user.email && childData.time == time && childData.read == false){
                        var Notice_msg = childData.email + "傳送新訊息!!!!";
                        var content = {
                            body: childData.data
                        }
                        var notification = new Notification(Notice_msg, content);
                    }
                }
            }
        });
        postsRef.on('child_changed', function(data){
            Read(0);
        });
    })
    .catch(e => console.log(e.message));
}

function Read(flag){
    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_chat = 0;
    // Counter for checking when to update new post
    var second_chat = 0;

    postsRef.once('value')
    .then(function(snapshot){
        var user = firebase.auth().currentUser;
        for(let i in snapshot.val()){
            var childData = snapshot.val()[i];
            var display;
            if(flag == 0){
                //alert(Check);
                //alert(childData.email +' '+ user.email +'\n'+ childData.to +' '+ now_chat_person);
                if(Check == 1 && childData.email == user.email && childData.to == now_chat_person){
                    //alert("hihi");
                    firebase.database().ref('com_list/'+ childData.key).update({
                        'read': true
                    });
                }
                if(childData.read == false || now_chat_person == 'public'){
                    display = ' ';
                }
                if(childData.read == true){
                    display = '已讀';
                }
            }
            else if(flag == 1){
                if(Check == 1 && childData.email == now_chat_person && childData.to == user.email){
                    firebase.database().ref('com_list/'+ childData.key).update({
                        'read': true
                    });
                }
                if(childData.read == false || now_chat_person == 'public'){
                    display = ' ';
                }
                if(childData.read == true){
                    display = '已讀';
                }
            }
            
            // The html code for post
            var before_user_cmt = "<div class='my-2 rounded box-shadow'></h6><div class='Media_r'><span class='time_date_l'><span class='read_l' id='"+childData.key+"'>"+display+"</span>"
            var mid_user_cmt = "</span><p class='sent_msg media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var after_user_cmt = "</p></div></div>\n";

            var before_client_cmt = "<div class='my-2 rounded box-shadow'></h6><div class='Media_l'><p class='received_msg media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var mid_client_cmt = "</p><span class='time_date_r'><span class='read_r' id='"+childData.key+"'></span>";
            var after_client_cmt = "</span></div></div>\n";

            var check1 = (childData.to == now_chat_person && childData.email == user.email);
            var check2 = (childData.email == now_chat_person && childData.to == user.email);
            if(Public && childData.to == now_chat_person){
                first_chat++;
                if(childData.email != user_email){
                    total_post[total_post.length] = before_client_cmt + childData.email + "</strong>" + childData.data + mid_client_cmt + childData.time + after_client_cmt;
                }
                else{
                    total_post[total_post.length] = before_user_cmt + childData.time + mid_user_cmt + childData.email + "</strong>" + childData.data + after_user_cmt;
                }
            }
            else if(check1 || check2){
                first_chat++;
                if(childData.email == user_email){
                    total_post[total_post.length] = before_user_cmt + childData.time + mid_user_cmt + childData.email + "</strong>" + childData.data + after_user_cmt;
                }
                else{
                    total_post[total_post.length] = before_client_cmt + childData.email + "</strong>" + childData.data + mid_client_cmt + childData.time + after_client_cmt;
                }
            }
            document.getElementById('msg_list').innerHTML = total_post.join('');
            var mesgs = document.getElementById('mesgs');
            mesgs.scrollTop = mesgs.scrollHeight;
        }
    })
    .catch(e => console.log(e.message));
}

function Chat(flag){
    // the person who registered
    var chatRef = firebase.database().ref('chat_list/');
    // List for store posts html
    var total_chat = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    chatRef.once('value')
    .then(function(snapshot){
        //document.getElementById('chat_people').innerHTML = total_chat.join('');
        var user = firebase.auth().currentUser;
        Check = 0;
        for(let i in snapshot.val()){
            var childData = snapshot.val()[i];
            var img = childData.picture;
            
            if(flag == 1){
                //alert("first: "+childData.user +' '+now_chat_person + '\n' + "second: " + childData.chat_with +' '+ user.email);
                if(childData.user == now_chat_person && childData.chat_with == user.email){
                    Check = 1;
                    //alert(Check);
                }
            }
            if(img == 'undefined' || img == '') img = 'img/unknown.png';
            else img = 'img/' + img;

            var str_before_clientname = "<div class='chat_list active_chat' id='chat_people'><div class='chat_people'><div class='chat_img'> <img src='"+ img +"'></div><div class='chat_ib'><h5 id='clientname'>";
            var str_after_clientname = "</h5><p id='chatcmt'></p>";
            var str_clientname = "</div></div></div>";
            var str_btn = "<button class='blue-pill' id='button' onclick='Changeperson(this,1)' value='"+childData.user+"'>chat</button>";
            if(childData.email != user.email){
                total_chat[total_chat.length] = str_before_clientname + childData.user + str_after_clientname + str_btn + str_clientname;
            }
            document.getElementById('chat_people').innerHTML = total_chat.join('');
        }
    })
    .catch(e => console.log(e.message));
}

function ResetPassword(){
    var auth = firebase.auth();
    var Email = auth.currentUser.email;

    auth.sendPasswordResetEmail(Email).then(function() {
        auth.signOut().then(function(result){
            alert('success reset :)');
            window.location.href = "index.html";
        })
        .catch(function(error){
            alert('QAQ');
        })
    }).catch(function(error) {
        alert("Reset fail!! QAQ")
    });
}

function converthtml(str)
{
    if(str == '') return;
    str = str.replace(/&/g, "&amp;");
    str = str.replace(/>/g, "&gt;");
    str = str.replace(/</g, "&lt;");
    str = str.replace(/"/g, "&quot;");
    str = str.replace(/'/g, "&#039;");
    return str;
}

function Get_Time(){
    var now_time = new Date();
    var hour = now_time.getHours();
    var minute = now_time.getMinutes();

    if(hour < 10){
        hour = '0' + hour;
    }
    if(minute < 10){
        minute = '0' + minute;
    }

    return (hour + ':' + minute);
}

window.onload = function() {
    init();
};